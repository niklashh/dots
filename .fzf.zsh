# Setup fzf
# ---------
if [[ ! "$PATH" == */home/niklash/dots/fzf/bin* ]]; then
  export PATH="${PATH:+${PATH}:}/home/niklash/.dots/fzf/bin"
fi

# Auto-completion
# ---------------
[[ $- == *i* ]] && source "/home/niklash/.dots/fzf/shell/completion.zsh" 2> /dev/null

# Key bindings
# ------------
source "/home/niklash/.dots/fzf/shell/key-bindings.zsh"
