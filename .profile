export PS2=':D '
export EDITOR="emacs -nw"
export TERM=xterm-256color
export N_PREFIX="$HOME/.local/"
export PATH="$HOME/.cargo/bin:$HOME/bin:$HOME/.local/bin:$PATH"
export DOCKER_HOST=unix:///run/user/1000/docker.sock
export MOZ_ENABLE_WAYLAND=1
export TERM=screen-256color
export RUST_BACKTRACE=1
export RUSTC_WRAPPER=sccache

[[ -e ~/.zsh_aliases ]] && source ~/.zsh_aliases
source "$HOME/.cargo/env"

export PATH="$HOME/.elan/bin:$PATH"
