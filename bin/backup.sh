#!/bin/sh

rsync -avP --delete /home/niklash/ \
  --exclude="Downloads" \
  --exclude="VirtualBox VMs" \
  --exclude="VMs" \
  --exclude=".cache" \
  --exclude=".local" \
  --exclude=".log" \
  --exclude=".rustup" \
  --exclude="*.iso" \
  -e "ssh -i $HOME/.ssh/id_rsa_freenas" \
  niklash@192.168.0.13:~/backups/
