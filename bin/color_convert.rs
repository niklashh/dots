// cargo-deps: palette="0.6", structopt="0.3"

use palette::{convert::IntoColorUnclamped, rgb::Rgb, Hsv, RgbHue, Srgb};
use structopt::StructOpt;

trait AsHexColor: IntoColorUnclamped<Srgb> {
    fn as_hex_color(self) -> String {
        let rgb: Srgb = self.into_color_unclamped();

        fn as_int(f: f32) -> u32 {
            (f * 255.0).round() as u32
        }

        format!(
            "{:02X}{:02X}{:02X}",
            as_int(rgb.red),
            as_int(rgb.green),
            as_int(rgb.blue)
        )
    }
}

impl<T: IntoColorUnclamped<Srgb>> AsHexColor for T {}

#[derive(Debug)]
struct ParseComponentError(u32);

impl std::fmt::Display for ParseComponentError {
    fn fmt(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            formatter,
            "Failed to parse color component: `{}` is outside of allowed range [0-255]",
            self.0
        )
    }
}

impl std::error::Error for ParseComponentError {}

fn parse_color(src: &str) -> Result<f32, Box<dyn std::error::Error>> {
    match src.parse::<u32>()? {
        num if 0 <= num && num <= 255 => Ok(num as f32 / 255.0),
        num => Err(Box::new(ParseComponentError(num))),
    }
}

#[derive(Debug, StructOpt)]
#[structopt(name = "color_convert", about = "Convert HSV to RGB")]
struct Opt {
    /// The hue value in degrees
    hue: f32,
    /// The saturation value between 0-255
    #[structopt(parse(try_from_str = parse_color))]
    saturation: f32,
    /// The 'value' value between 0-255
    #[structopt(parse(try_from_str = parse_color))]
    value: f32,
}

fn main() {
    let opt = Opt::from_args();
    let color = Hsv::new(RgbHue::from_degrees(opt.hue), opt.saturation, opt.value);
    println!("{}", color.as_hex_color());
}
