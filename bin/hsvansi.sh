#!/bin/bash

# 30 hue values
RGB=(`seq 196 6 220 \
  && seq 226 -36 82 \
  && seq 46 1 50 \
  && seq 51 -6 27 \
  && seq 21 36 165 \
  && seq 201 -1 197`)

if [ -z $1 ]; then
  for color in ${RGB[@]}; do
    printf "\033[48;5;${color}m \033[0m"
  done
  echo "
Prints the ansi color for a give hue value
USAGE: $0 <value between [0, 30[>"
  exit 1
fi

printf ${RGB[$1]}
