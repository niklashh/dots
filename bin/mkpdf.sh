#!/usr/bin/env bash

name=$1

[[ "$name" == "" ]] && { echo "missing argument NAME"; exit 1; }

shift

convert -resize 1611x1611 $@ `date --iso-8601`"_${name}_Niklas_Halonen_674546.pdf"
