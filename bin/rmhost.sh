LINE=$1

RE='^[0-9]+$'
if [ -z "${LINE}" ]; then
  echo "Usage: $0 N:integer"
  echo "  Delete the Nth line in .ssh/known_hosts"
  exit 1
elif ! [[ "${LINE}" =~ ${RE} ]]; then
  echo "${LINE} is not a number!"
  exit 1
else
  cat ~/.ssh/known_hosts | sed "${LINE}d" | tee ~/.ssh/known_hosts > /dev/null
fi
