#!/bin/bash

set -x

jackdpid=''
jackconfigpid=''
function exit()
{
    kill $jackdpid $jackconfigpid
    systemctl --user stop pulseaudio.socket
}

trap exit SIGINT

systemctl --user stop pulseaudio.socket
pkill jackd

sleep 1
jackd -d dummy -r 44100 &
jackdpid="$!"
sleep 1
# alsa_in -v -d hw:He -j 'Headphones in' &
# alsa_out -v -d hw:He -j 'Headphones out' &
# alsa_in -v -d hw:USB -j 'Guitar in' &
# alsa_out -v -d hw:USB -j 'Guitar out' &

sleep 1
systemctl --user start pulseaudio.socket
pactl load-module module-jack-sink channels=2 && pactl load-module module-jack-source && pacmd set-default-sink jack_out

# guitarix --mute --name='Guitar amp' --jack-input='Guitar in:capture_2' --jack-output='Headphones out:playback_1' --jack-output='Headphones out:playback_2' &

sleep 1

# jack-config compressor &
# jackconfigpid="$!"

wait
