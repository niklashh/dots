#!/bin/bash -ex

cd ~/.mozilla/firefox/*.default-release/

mkdir -p chrome

cat <<-EOF > chrome/userChrome.css
#main-window[tabsintitlebar="true"]:not([extradragspace="true"]) #TabsToolbar > .toolbar-items {
  opacity: 0;
  pointer-events: none;
}
#main-window:not([tabsintitlebar="true"]) #TabsToolbar {
    visibility: collapse !important;
}
EOF

#ADDON=ublock_origin-1.43.0.xpi
#TMPDIR=`mktemp -d`
#wget https://addons.mozilla.org/firefox/downloads/file/3961087/$ADDON -O "$TMPDIR/$ADDON"
#unzip "$TMPDIR/$ADDON" -d "$TMPDIR"


wget https://addons.mozilla.org/firefox/downloads/file/3961087/$ADDON -O uBlock0@raymondhill.net.xpi
