#!/bin/bash -e

DOTS=`( cd \`dirname $0\` && pwd )`

# Reset in case getopts has been used previously in the shell.
OPTIND=1

# Initialize our own variables:
verbose=false
default="none"

while getopts "vyn" opt; do
    case "$opt" in
    v)  verbose=true
        ;;
    y)  default="yes"
        ;;
    n)  [ "$default" = "yes" ] && { echo "yes and no are mutually exclusive!" >&2; exit 1; }
        default="no"
        ;;
    esac
done

shift $((OPTIND-1))

[ "${1:-}" = "--" ] && shift

echocolor() {
  local COLOR=$1
  shift
  local TEXT=$@
  echo -e "\e[${COLOR}m${TEXT}\e[0m"
}

debug() {
  [ "$verbose" = true ] && echocolor 94 "DEBUG: $@"
}

debug "dots path $DOTS"

if [ -z "$DOTS" ]; then
  # error; for some reason, the path is not accessible
  # to the script (e.g. permissions re-evaled after suid)
  echo "$0 not accessible"
  exit 1
fi

remove() {
  if [ "$default" = "yes" ]; then
    # non-interactive remove
    rm $@
  elif [ "$default" = "no" ]; then
    # do nothing
    debug "Not removing, args: $@"
  else
    # interactive remove
    rm -i $@
  fi
}

install() {
  debug "install $1"
  if [ -L "$1" ]; then
    if readlink -e $1; then
      L=`readlink -e $1`
      debug "readlink $L"
      if [[ "$L" == "$2" ]]; then
        echocolor 92 "$1 already linked"
      else
        echocolor 91 "$1 is linked to $L which is not $2!"
        remove $1
      fi
    else
      echocolor 91 "$1 is an invalid link!"
      remove $1
    fi
  else
    if [ -f "$1" ]; then
      echocolor 93 "File $1 already exists!"
      remove $1
    elif [ -d "$1" ]; then
      echocolor 93 "Directory $1 already exists!"
      remove -r $1
    fi
  fi
  if [[ ! -f "$1" && ! -d "$1" ]]; then
    echocolor 92 "Linking $1 => $2"
    mkdir -p `dirname $1`
    ln -s $2 $1
  fi
}

# shell
install ~/bin $DOTS/bin
install ~/.tmux.conf $DOTS/.tmux.conf
install ~/.config/fish $DOTS/fish

# .config
install ~/.config/alacritty $DOTS/alacritty
install ~/.emacs.d $DOTS/vim
install ~/.config/ranger $DOTS/ranger
install ~/.config/sway $DOTS/sway
install ~/.config/i3 $DOTS/i3
install ~/.config/i3blocks $DOTS/i3blocks
install ~/.config/gtk-3.0 $DOTS/gtk-3.0
install ~/.config/systemd $DOTS/systemd
install ~/.ssh/config $DOTS/ssh/config

# git
install ~/.gitconfig $DOTS/.gitconfig
install ~/.gitignore_global $DOTS/.gitignore_global

# apps
install ~/.PrusaSlicer $DOTS/prusa-slicer
install ~/.FreeCAD $DOTS/dotfreecad

# wm
# install ~/.xinitrc $DOTS/.xinitrc
# install ~/.xprofile $DOTS/.xprofile
