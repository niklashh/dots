set -x PATH $PATH ~/.cargo/bin/ ~/.local/bin ~/bin ~/.idris2/bin /sbin/ ~/.elan/bin

function git_current_branch -d 'prints the current branch or nothing if not on a branch'
    set -l ref (command git symbolic-ref --quiet HEAD 2> /dev/null)
    set -l ret $status
    if [ $ret -ne 0 ]
       # not on a branch
       return
    end
    echo (string replace 'refs/heads/' '' $ref)
end

function git_main_branch -d 'prints the main branch of the repository'
    command git rev-parse --git-dir &>/dev/null; or return
    for ref in refs/{heads,remotes/{origin,upstream}}/{main,trunk}
        if command git show-ref -q --verify "$ref"
            echo (string replace 'refs/heads/' '' $ref)
            return
        end
    end
    echo master
end

function git_count_commits_per_file -d 'lists files with most commits sorted' -a 'ref' --wraps 'git log'
    if test -z "$ref"
        set ref (git_current_branch)
    end
    git log --name-only --pretty=format: "$ref" | sort | uniq -c | sort -n
end

abbr --add gccpf 'git_count_commits_per_file'

function hexdelta -d 'Prints the delta between hexdumps of files'
    delta (hexdump -C $argv[1] | psub) (hexdump -C $argv[2] | psub)
end

function orgpdf -d 'org-latex-export-pdf the input org document'
    emacs -u (id -un) --batch --eval '(load user-init-file)' $argv -f org-latex-export-to-pdf
end

# exa
alias ls="exa"
alias l='ls -al --group-directories-first --git --sort time'

# systemctl
abbr --add s "systemctl"
abbr --add sr "systemctl restart"
abbr --add sru "systemctl --user restart"
abbr --add sen "systemctl enable --now"
abbr --add senu "systemctl --user enable --now"
abbr --add stop "systemctl stop"
abbr --add stopu "systemctl --user stop"
abbr --add sst "systemctl status"
abbr --add sstu "systemctl --user status"
abbr --add j "journalctl -eu"
abbr --add ju "journalctl --user -eu"

# various others
abbr --add dush "du -sh"
abbr --add b 'bat'
abbr --add bjs 'bat -l javascript'
abbr --add bmd 'bat -l md'
abbr --add qr 'qrencode -m 2 -t utf8'
abbr --add cx 'chmod +x'
abbr --add md 'mkdir'
abbr --add mdp 'mkdir -p'
abbr --add sus 'systemctl suspend'
abbr --add lb 'lsblk -o name,mountpoint,label,size,uuid'
abbr --add cp 'cp -v'
abbr --add mv 'mv -v'

# change to grand parent directory
# e.g. ... => ../../../
for i in (seq 3 10)
    set -l shortcut (string repeat -n $i .)
    set -l path (string repeat -n $i ../)
    echo $shortcut $path
end

# docker/docker-compose
alias docker='podman'
alias docker-compose='podman-compose'
abbr --add p 'podman'
abbr --add d 'docker'
abbr --add dr 'docker run'
abbr --add drr 'docker run --rm'
abbr --add dv 'docker volume'
abbr --add di 'docker image'
abbr --add dpaq 'docker ps -a -q'
abbr --add drv 'docker rm -v'
abbr --add de 'docker exec'
abbr --add deit 'docker exec -it'
abbr --add dc 'docker-compose'
abbr --add pc 'podman-compose'
abbr --add dsa 'docker stop `docker ps -q`'
abbr --add dsaq 'docker stop `docker ps -a -q`'
abbr --add drm 'docker rm'
abbr --add dresh 'docker run --rm -it --entrypoint /bin/sh'
abbr --add drebash 'docker run --rm -it --entrypoint /bin/bash'

# kubectl shortcuts
abbr --add k 'kubectl'
abbr --add kg 'kubectl get'
abbr --add kd 'kubectl describe'
abbr --add ka 'kubectl apply'
abbr --add ke "kubectl get events --sort-by='.lastTimestamp'"

# cargo shortcuts
abbr --add c 'cargo'
abbr --add cr 'cargo run'
abbr --add cre 'cargo run --example'
abbr --add cb 'cargo build'
abbr --add ct 'cargo test'
abbr --add cc 'cargo clean'
abbr --add ca 'cargo add'
abbr --add crm 'cargo rm'
abbr --add cbh 'cargo bench'
abbr --add ce 'cargo expand'
abbr --add csra 'cargo +nightly -Z sparse-registry add'

# git shortcuts https://github.com/ohmyzsh/ohmyzsh/blob/master/plugins/git/git.plugin.zsh
abbr --add g 'git'
abbr --add ga 'git add'
abbr --add gaa 'git add --all'
abbr --add gapa 'git add --patch'
abbr --add gau 'git add --update'
abbr --add gav 'git add --verbose'
abbr --add gac 'git add (git rev-parse --show-toplevel)/Cargo.{toml,lock}'
abbr --add gap 'git apply'
abbr --add gapt 'git apply --3way'
abbr --add gb 'git branch'
abbr --add gba 'git branch -a'
abbr --add gbd 'git branch -d'
abbr --add gbD 'git branch -D'
abbr --add gbl 'git blame -b -w'
abbr --add gbnm 'git branch --no-merged'
abbr --add gbr 'git branch --remote'
abbr --add ggsup 'git branch --set-upstream-to origin/(git_current_branch)'
abbr --add gbf 'git branch -f'
abbr --add gbfmom 'git branch -f (git_main_branch) origin/(git_main_branch)'
abbr --add gbs 'git bisect'
abbr --add gbsb 'git bisect bad'
abbr --add gbsg 'git bisect good'
abbr --add gbsr 'git bisect reset'
abbr --add gbss 'git bisect start'
abbr --add gc 'git commit'
abbr --add gc! 'git commit --amend'
abbr --add gcn! 'git commit --no-edit --amend'
abbr --add gca 'git commit -a'
abbr --add gca! 'git commit -a --amend'
abbr --add gcan! 'git commit -a --no-edit --amend'
abbr --add gcans! 'git commit -a -s --no-edit --amend'
abbr --add gcam 'git commit -a -m'
abbr --add gcsm 'git commit -s -m'
abbr --add gcas 'git commit -a -s'
abbr --add gcasm 'git commit -a -s -m'
abbr --add gcb 'git checkout -b'
abbr --add gcf 'git config --list'
abbr --add gcl 'git clone --recurse-submodules'
abbr --add gclean 'git clean -id'
abbr --add gpristine 'git reset --hard && git clean -dffx'
abbr --add gcm 'git checkout (git_main_branch)'
abbr --add gcmsg 'git commit -m'
abbr --add gco 'git checkout'
abbr --add gcor 'git checkout --recurse-submodules'
abbr --add gcount 'git shortlog -sn'
abbr --add gcp 'git cherry-pick'
abbr --add gcpa 'git cherry-pick --abort'
abbr --add gcpc 'git cherry-pick --continue'
abbr --add gcs 'git commit -S'
abbr --add gcss 'git commit -S -s'
abbr --add gcssm 'git commit -S -s -m'
abbr --add gd 'git diff'
abbr --add gdca 'git diff --cached'
abbr --add gdcw 'git diff --cached --word-diff'
abbr --add gdct 'git describe --tags (git rev-list --tags --max-count 1)'
abbr --add gds 'git diff --staged'
abbr --add gdt 'git diff-tree --no-commit-id --name-only -r'
abbr --add gdup 'git diff @{upstream}'
abbr --add gdw 'git diff --word-diff'
abbr --add gf 'git fetch'
abbr --add gfa 'git fetch --all --prune --jobs 10'
abbr --add gfo 'git fetch origin'
abbr --add gfg 'git ls-files | grep'
abbr --add gg 'git gui citool'
abbr --add gga 'git gui citool --amend'
abbr --add ggpull 'git pull origin "(git_current_branch)"'
abbr --add ggpush 'git push origin "(git_current_branch)"'
abbr --add gpsup 'git push --set-upstream origin (git_current_branch)'
abbr --add ghh 'git help'
abbr --add gignore 'git update-index --assume-unchanged'
abbr --add gignored 'git ls-files -v | grep "^[[:lower:]]"'
abbr --add git-svn-dcommit-push 'git svn dcommit && git push github (git_main_branch):svntrunk'
abbr --add gl 'git pull'
abbr --add glg 'git log --stat'
abbr --add glgp 'git log --stat -p'
abbr --add glgg 'git log --graph'
abbr --add glgga 'git log --graph --decorate --all'
abbr --add glgm 'git log --graph --max-count 10'
abbr --add glo 'git log --oneline --decorate'
abbr --add glol "git log --graph --pretty='%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ar) %C(bold blue)<%an>%Creset'"
abbr --add glols "git log --graph --pretty='%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ar) %C(bold blue)<%an>%Creset' --stat"
abbr --add glod "git log --graph --pretty='%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ad) %C(bold blue)<%an>%Creset'"
abbr --add glods "git log --graph --pretty='%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ad) %C(bold blue)<%an>%Creset' --date short"
abbr --add glola "git log --graph --pretty='%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ar) %C(bold blue)<%an>%Creset' --all"
abbr --add glolas "git log --graph --pretty='%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --all --stat"
abbr --add glolad "git log --graph --pretty='%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ar) %C(bold blue)<%an>%Creset' --all --date-order"
abbr --add glolasd "git log --graph --pretty='%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ar) %C(bold blue)<%an>%Creset' --all --stat --date-order"
abbr --add glog 'git log --oneline --decorate --graph'
abbr --add gloga 'git log --oneline --decorate --graph --all'
abbr --add gm 'git merge'
abbr --add gmom 'git merge origin/(git_main_branch)'
abbr --add gmtl 'git mergetool --no-prompt'
abbr --add gmtlvim 'git mergetool --no-prompt --tool vimdiff'
abbr --add gmum 'git merge upstream/(git_main_branch)'
abbr --add gma 'git merge --abort'
abbr --add gp 'git push'
abbr --add ggp 'git push origin (git_current_branch)'
abbr --add gpd 'git push --dry-run'
abbr --add gpf 'git push --force-with-lease'
abbr --add gpf! 'git push --force'
abbr --add gpoat 'git push origin --all && git push origin --tags'
abbr --add gpr 'git pull --rebase'
abbr --add gpu 'git push upstream'
abbr --add gpv 'git push -v'
abbr --add gpod 'git push origin --delete'
abbr --add gpt 'git push --tags'
abbr --add gptf 'git push --tags -f'
abbr --add gr 'git remote'
abbr --add gra 'git remote add'
abbr --add grb 'git rebase'
abbr --add grba 'git rebase --abort'
abbr --add grbc 'git rebase --continue'
abbr --add grbi 'git rebase -i'
abbr --add grbm 'git rebase (git_main_branch)'
abbr --add grbo 'git rebase --onto'
abbr --add grbs 'git rebase --skip'
abbr --add grev 'git revert'
abbr --add grh 'git reset'
abbr --add grhh 'git reset --hard'
abbr --add groh 'git reset origin/(git_current_branch) --hard'
abbr --add grm 'git rm'
abbr --add grmc 'git rm --cached'
abbr --add grmv 'git remote rename'
abbr --add grrm 'git remote remove'
abbr --add grs 'git restore'
abbr --add grset 'git remote set-url'
abbr --add grss 'git restore --source'
abbr --add grst 'git rev-parse --show-toplevel'
abbr --add grt 'cd "(git rev-parse --show-toplevel || echo .)"'
abbr --add gru 'git reset --'
abbr --add grup 'git remote update'
abbr --add grv 'git remote -v'
abbr --add gsb 'git status -sb'
abbr --add gsd 'git svn dcommit'
abbr --add gsh 'git show'
abbr --add gsi 'git submodule init'
abbr --add gsps 'git show --pretty short --show-signature'
abbr --add gsr 'git svn rebase'
abbr --add gss 'git status -s'
abbr --add gst 'git status'
abbr --add gsta 'git stash push'
abbr --add gstaa 'git stash apply'
abbr --add gstaa 'git stash apply --index'
abbr --add gstspa 'git stash save --patch'
abbr --add gstc 'git stash clear'
abbr --add gstd 'git stash drop'
abbr --add gstl 'git stash list'
abbr --add gstp 'git stash pop'
abbr --add gsts 'git stash show --text'
abbr --add gstu 'gsta --include-untracked'
abbr --add gstall 'git stash --all'
abbr --add gsa 'git submodule add'
abbr --add gsu 'git submodule update'
abbr --add gsur 'git submodule update --remote'
abbr --add gsw 'git switch'
abbr --add gswc 'git switch -c'
abbr --add gts 'git tag -s'
abbr --add gtv 'git tag | sort -V'
abbr --add gunignore 'git update-index --no-assume-unchanged'
abbr --add gunwip 'git log -n 1 | grep -q -c "\-\-wip\-\-" && git reset HEAD~1'
abbr --add gup 'git pull --rebase'
abbr --add gupv 'git pull --rebase -v'
abbr --add gupa 'git pull --rebase --autostash'
abbr --add gupav 'git pull --rebase --autostash -v'
abbr --add glum 'git pull upstream (git_main_branch)'
abbr --add gwch 'git whatchanged -p --abbrev-commit --pretty medium'
abbr --add gwip 'git add -A; git rm (git ls-files --deleted) 2> /dev/null; git commit --no-verify --no-gpg-sign -m "--wip-- [skip ci]"'
abbr --add gam 'git am'
abbr --add gamc 'git am --continue'
abbr --add gams 'git am --skip'
abbr --add gama 'git am --abort'
abbr --add gamscp 'git am --show-current-patch'

# gitflow shortcuts
abbr --add gfrs 'git flow release start'
abbr --add gfrf 'git flow release finish'
abbr --add gffs 'git flow feature start'
abbr --add gfff 'git flow feature finish'

# vagrant shortcuts
abbr --add v 'vagrant'
abbr --add vu 'vagrant up'
abbr --add vst 'vagrant status'
abbr --add vb 'vagrant box'
abbr --add vinit 'vagrant init'
abbr --add vr 'vagrant reload'
abbr --add vp 'vagrant provision'
abbr --add vrp 'vagrant reload --provision'
abbr --add vs 'vagrant ssh'
abbr --add vd 'vagrant destroy'

# ranger shortcuts
abbr --add r 'ranger'

# terraform shortcuts
abbr --add tf 'terraform'
abbr --add tfi 'terraform init'
abbr --add tfp 'terraform plan'
abbr --add tfa 'terraform apply'
abbr --add tfmt 'terraform fmt'
abbr --add tfs 'terraform show'
abbr --add tfd 'terraform destroy'

# toolbox shortcuts
abbr --add t 'toolbox'
abbr --add tc 'toolbox create'
abbr --add te 'toolbox enter'
abbr --add trm 'toolbox rm -f'

if status is-interactive
    set -U fish_greeting
    # FIXME remove old setup
    # set -x EDITOR "emacs -nw"
    # set -x EDITOR "TERM=xterm-emacs emacsclient -t"
    # set -x TERM "screen-256color"
    
    set -x TERM xterm-direct
    set -x EDITOR "emacsclient -t"

    # editor shortcuts
    alias vim=$EDITOR

    fish_ssh_agent

    type -q tmux && [ -z "$TMUX" ] && [ (hostname) != "toolbox" ] && tmux -2
    clear
end
