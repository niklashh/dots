#!/bin/sh

# exit when any command fails
set -e

mkdir -p $HOME/.fonts

echo "Downloading comic-mono"
curl -L https://dtinth.github.io/comic-mono-font/ComicMono.ttf -o $HOME/.fonts/ComicMono.ttf

fc-cache -f

echo "Comic-mono installed successfully"
