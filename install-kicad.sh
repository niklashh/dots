#!/bin/sh -e

BASEURL="https://kicad-downloads.s3.cern.ch/"
NIGHTLY_KICAD=`curl -s "${BASEURL}?delimiter=/&prefix=appimage/nightly/" |xmllint --xpath 'string(//*[local-name()="Key"])' -`
KICAD="${NIGHTLY_KICAD/#appimage\/nightly\//}"

echo "Installing ${KICAD} to $HOME/.local/bin/kicad"
mkdir -p $HOME/.local/bin
wget "${BASEURL}${NIGHTLY_KICAD}" -O ~/.local/bin/kicad
chmod +x ~/.local/bin/kicad

echo "${KICAD} installed successfully"
