#!/usr/bin/env bash

set -ex

curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

source $HOME/.cargo/env

cargo install alacritty exa bat cargo-eval cargo-edit cargo-watch git-delta
cargo +nightly install cargo-expand

# TODO rust analyzer
