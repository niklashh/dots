#!/bin/sh

# exit when any command fails
set -e

echo "Downloading Telegram"
curl -L https://telegram.org/dl/desktop/linux -o /tmp/telegram.tar.xz
mkdir -p $HOME/.telegram
echo "Extracting Telegram"
tar -xvJf /tmp/telegram.tar.xz -C $HOME/.telegram --strip-components 1
rm /tmp/telegram.tar.xz

echo "Installing Telegram to $HOME/.local/bin/telegram"
mkdir -p $HOME/.local/bin
ln -sf $HOME/.telegram/Telegram $HOME/.local/bin/telegram

echo "Telegram installed successfully"
